# Case study

insert_case_studies.sql

## Case study 1 : Detection of acute renal failure during the hospital stay 

- 5 persons (1 without measurement)
- person 1004 has two hospital stay
- visit 1003 has two visit_detail



Period : insert_period_case_study_1.sql 
Period_concept_id = 32037
6 periods

#### Max of creatinin

| Person | Visit occurrence | Visit detail | Feature | Expected value |
| -------- | -------- | -------- | -------- | -------- |
| 1000    | 1000     | 1000     | Max | 40 |
| 1001    | 1001     | 1001     | Max | 2000 |
| 1002    | 1002     | 1002     | Max | Pas de mesure |
| 1003    | 1003     | 1003     | Max | 35 |
| 1003    | 1003     | 1004     | Max | 26 |
| 1004    | 1004     | 1005     | Max | 36 |
| 1004    | 1005     | 1006     | Max | 46 |

#### Max of creatinin without extreme values

| Person | Visit occurrence | Visit detail | Feature | Expected value |
| -------- | -------- | -------- | -------- | -------- |
| 1000    | 1000     | 1000     | Max | 40 |
| 1001    | 1001     | 1001     | Max | -- |
| 1002    | 1002     | 1002     | Max | Pas de mesure |
| 1003    | 1003     | 1003     | Max | 35 |
| 1003    | 1003     | 1004     | Max | 26 |
| 1004    | 1004     | 1005     | Max | 36 |
| 1004    | 1005     | 1006     | Max | 46 |




## Case study 2 : Hypotension during the first post-operative ICU stay

TODO : 
- [ ] hypotension sur plusieurs jours


#### Pre-test : Max value

| Person | Visit occurrence | Visit detail | Feature | Expected value | 
| -------- | -------- | -------- | -------- | -------- |
| 2000    | 2000     | 2000     | Max | 77 |
| 2001    | 2001     | 2001     | Max | No ICU |
| 2002    | 2002     | 2002     | Max | 77 |
| 2002    | 2002     | 2003     | Max | No ICU |
| 2003    | 2003     | 2004     | Max | 70 |
| 2003    | 2003     | 2005     | Max | No ICU |
| 2003    | 2003     | 2006     | Max | 77 |
| 2004    | 2004     | 2007     | Max | 75 |
| 2004    | 2004     | 2008     | Max | No ICU |
| 2004    | 2005     | 2009     | Max | No ICU |
| 2004    | 2005     | 2010     | Max | 77 |

#### MAP < 65 mmHg

| Person | Visit occurrence | Visit detail | Feature | Expected value | 
| -------- | -------- | -------- | -------- | -------- |
| 2000    | 2000     | 2000     | Outside MAP < 65 | 1 |
| 2001    | 2001     | 2001     | Outside MAP < 65 | No ICU |
| 2002    | 2002     | 2002     | Outside MAP < 65 | 1 |
| 2002    | 2002     | 2003     | Outside MAP < 65 | No ICU |
| 2003    | 2003     | 2004     | Outside MAP < 65 | 1 |
| 2003    | 2003     | 2005     | Outside MAP < 65 | No ICU |
| 2003    | 2003     | 2006     | Outside MAP < 65 | 1 |
| 2004    | 2004     | 2007     | Outside MAP < 65 | 1 |
| 2004    | 2004     | 2008     | Outside MAP < 65 | No ICU |
| 2004    | 2005     | 2009     | Outside MAP < 65 | No ICU |
| 2004    | 2005     | 2010     | Outside MAP < 65 | 1 |

#### Duration MAP < 65 mmHg

| Person | Visit occurrence | Visit detail | Feature | Expected value | 
| -------- | -------- | -------- | -------- | -------- |
| 2000    | 2000     | 2000     | Duration MAP < 65 | 60 min |
| 2001    | 2001     | 2001     | Duration MAP < 65 | No ICU |
| 2002    | 2002     | 2002     | Duration MAP < 65 | 50 min |
| 2002    | 2002     | 2003     | Duration MAP < 65 | No ICU |
| 2003    | 2003     | 2004     | Duration MAP < 65 | 30 |
| 2003    | 2003     | 2005     | Duration MAP < 65 | No ICU |
| 2003    | 2003     | 2006     | Duration MAP < 65 | 120 |
| 2004    | 2004     | 2007     | Duration MAP < 65 | 45 |
| 2004    | 2004     | 2008     | Duration MAP < 65 | No ICU |
| 2004    | 2004     | 2009     | Duration MAP < 65 | No ICU |
| 2004    | 2004     | 2010     | Duration MAP < 65 | 120 |


## Case study 3 : Psychiatric scale after a visit (or new treatment)

| Person | Visit occurrence | Visit detail | Feature | Expected value | 
| -------- | -------- | -------- | -------- | -------- |
| 3000    | 3000     | 3000     |  |  |

## Case study 4 : Duration of hypotension during general anesthesia

| Person | Visit occurrence | Visit detail | Feature | Expected value | 
| -------- | -------- | -------- | -------- | -------- |
| 4000    | 4000     | 4000     |  |  |

## Case study 5 : Measures between the administration of a drug and a procedure

| Person | Visit occurrence | Visit detail | Feature | Expected value | 
| -------- | -------- | -------- | -------- | -------- |
| 5000    | 5000     | 5000     |  |  |


# TODO

### crossingThreshold
- [ ] vérifier les arguments
- [ ] passer l'opérateur en dynamique


DROP SEQUENCE IF EXISTS @featureDatabaseSchema.seq ;

CREATE SEQUENCE @featureDatabaseSchema.seq START 1 ;

INSERT INTO @featureDatabaseSchema.period (period_id, 
                    person_id, 
                    period_concept_id, 
                    period_start_datetime,
                    period_end_datetime, 
                    visit_occurrence_id, 
                    visit_detail_id)
SELECT  nextval('feature.seq'),
        person_id,
        @periodId,
		coalesce (vd.visit_detail_start_datetime, vd.visit_detail_start_date),
		coalesce (vd.visit_detail_end_datetime, vd.visit_detail_end_date),
        visit_occurrence_id,
        visit_detail_id
from @cdmDatabaseSchema.visit_detail vd
where visit_detail_concept_id = 32037
;
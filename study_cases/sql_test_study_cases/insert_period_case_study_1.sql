


DROP SEQUENCE IF EXISTS @featureDatabaseSchema.seq ;

CREATE SEQUENCE @featureDatabaseSchema.seq START 1 ;

DELETE FROM feature.period WHERE period_concept_id = @periodId;

INSERT INTO @featureDatabaseSchema.period (period_id, 
  person_id, 
  period_concept_id, 
  period_start_datetime,
  period_end_datetime, 
  visit_occurrence_id, 
  visit_detail_id)

SELECT nextval('feature.seq'),
	vo.person_id,
	@periodId,
	coalesce (vd.visit_detail_start_datetime, vd.visit_detail_start_date),
	coalesce (vd.visit_detail_end_datetime, vd.visit_detail_end_date),
	vo.visit_occurrence_id,
	vd.visit_detail_id

FROM @cdmDatabaseSchema.visit_occurrence vo
inner join @cdmDatabaseSchema.visit_detail vd
on vo.visit_occurrence_id = vd.visit_occurrence_id
WHERE vo.visit_concept_id = 9201 --- Inpatient
OR vo.visit_concept_id = 262 --- Emergency room & Inpatient
;
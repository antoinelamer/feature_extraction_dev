---------------------------------------------------------------------------------------------
--
-- Insert records in table feature in order to test featureWider() function
--
-- 2020-12-30
--
-- Antoine Lamer
--
---------------------------------------------------------------------------------------------


-- Patient 1, visit_detail_1
---------------------------------------------------------------------------------------------

INSERT INTO feature.feature
(feature_id, 
person_id, 
period_id, 
feature_concept_id, 
period_concept_id, 
raw_data_concept_id, 
feature_method_concept_id, 
value_as_number, 
value_as_concept_id, 
visit_occurrence_id, 
visit_detail_id, 
feature_type_concept_id)
VALUES(1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1);

INSERT INTO feature.feature
(feature_id, 
person_id, 
period_id, 
feature_concept_id, 
period_concept_id, 
raw_data_concept_id, 
feature_method_concept_id, 
value_as_number, 
value_as_concept_id, 
visit_occurrence_id, 
visit_detail_id, 
feature_type_concept_id)
VALUES(2, 1, 1, 2, 1, 1, 1, 4, 1, 1, 1, 1);

INSERT INTO feature.feature
(feature_id, 
person_id, 
period_id, 
feature_concept_id, 
period_concept_id, 
raw_data_concept_id, 
feature_method_concept_id, 
value_as_number, 
value_as_concept_id, 
visit_occurrence_id, 
visit_detail_id, 
feature_type_concept_id)
VALUES(3, 1, 1, 3, 1, 1, 1, 5, 1, 1, 1, 1);

-- Patient 2, visit_detail 2
---------------------------------------------------------------------------------------------

INSERT INTO feature.feature
(feature_id, 
person_id, 
period_id, 
feature_concept_id, 
period_concept_id, 
raw_data_concept_id, 
feature_method_concept_id, 
value_as_number, 
value_as_concept_id, 
visit_occurrence_id, 
visit_detail_id, 
feature_type_concept_id)
VALUES(4, 2, 1, 3, 1, 1, 1, 15, 1, 1, 2, 1);


---------------------------------------------------------------------------------------------

SELECT visit_detail_id, feature_concept_id,
                    value_as_number
          FROM feature.feature
          WHERE feature_concept_id in (1, 2, 3);

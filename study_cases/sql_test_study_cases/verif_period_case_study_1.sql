

SELECT    period.period_id,
                    period.person_id,
                    period.visit_detail_id,
                    measurement.measurement_datetime,
                    measurement.value_as_number,
                    measurement.visit_detail_id,
                    measurement.visit_occurrence_id
              FROM omop.measurement
              INNER JOIN feature.period
              ON period.visit_detail_id = measurement.visit_detail_id
              WHERE period_concept_id = 2000056759
              AND measurement_concept_id = 4108289
              AND measurement.measurement_datetime >= period.period_start_datetime
              AND measurement.measurement_datetime <= period.period_end_datetime ;
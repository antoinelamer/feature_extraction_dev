DROP SEQUENCE IF EXISTS feature.seq ;

CREATE SEQUENCE feature.seq START 1 ;

delete from feature.period where period_concept_id = 9201;

INSERT INTO feature.period (period_id, 
  person_id, 
  period_concept_id, 
  period_start_datetime,
  period_end_datetime, 
  visit_occurrence_id, 
  visit_detail_id)

SELECT nextval('feature.seq'),
	vo.person_id,
	9201,
	vo.visit_start_datetime,
	vo.visit_end_datetime,
	vo.visit_occurrence_id,
	null

FROM omop.visit_occurrence vo

WHERE vo.visit_concept_id = 9201 --- Inpatient
OR vo.visit_concept_id = 262 --- Emergency room & Inpatient
;
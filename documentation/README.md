

[TOC]

# Introduction

The increasing implementation and use of electronic health records (EHRs) over the last few decades has made a significant amount of clinical data available in electronic format [^Adler]. EHRs collect and restitute data for health care, administrative or billing purposes. In addition to these initial uses, they also offer opportunities for data reuse defined as “non-direct care use of personal health information" [^Safran]. Thus, data reuse offers possibilities for research, quality of care, activity management or public health [^Safran_2014].

Despite the many opportunities data reuse offers, its implementation presents many difficulties and primary data cannot be reused directly. First of all, data reuse encounters data quality problems that arise from the manner in which they were entered or collected. This results in a phase of data cleaning to deduplicate, filter, homogenize or convert raw data. Moreover, information are not always directly available in the source database, and have to be computed afterwards with raw data in defining a algorithm [^Lamer_2016]. This is generally named “data transformation” [^Fayyad],  “data aggregation” [^Lamer_2016_2], or “feature extraction” [^Chazard_2018].

When performing prospective studies, inclusion criteria and variable collection (i.e. outcomes, exposures and adjusting variables) are defined upstream. In routine, they are collected manually with human expertise, one recording at a time and in taking into account the background. If needed, it is possible to query third party data sources or  caregivers expertise.

In contrast, in data reuse, feature are extracted automatically from a static database (already saved and closed), for patients for whom the care event has already been completed years earlier and for a large number of records. All scenarios must be taken into consideration so as to avoid having to modify records individually and by hand. 

We have developped some methods according to our needs and experiences [^Lamer_2016_2][^Chazard_2009]. Four dimensions have been extrapolated from these methods to characterize feature extraction : the statistical unit of the records (e.g. the patient, the hospital stay, the procedure), the period of interest (e.g. the 2 days following a procedure), the signal (e.g. the heat rate, the creatinin, or a psychatric scale) and the method of extraction (e.g. a statistic indicator or a expert-defined rule). 

The purpose of this article is to present the generalisation of feature extraction methods. A package is proposed to allow extraction methods to be applied on raw data. Several methods have been implemented to meet the needs of clinicians and experts. We defined to new tables for storing feature in the OMOP CDM.

# Study cases

We collected study cases with experts from several fields: anesthesiology, intensive care, psychiatry. These study cases were related to retrospective studies, and had required automatic management of raw data to provide useful information for statistical analysis, data visualisation or another form of usage.

## Study case 1 : Detection of acute renal failure during the hospital stay

The acute renal failure is detected with creatinine values (biology).

![](https://gitlab.com/antoinelamer/omop_anesthesia/-/raw/master/images/case_stud_AKI_creatinin.png)



| Feature parameters | Values | 
| -------- | -------- | 
| Statistical unit     | Hospital stay and 1st surgical procedure     |
| Period     | 3 days following surgical procedure     |
| Signal     | Creatinin     |
| Extraction method    | KDIGO     |


[SQL code for periods creation](https://gitlab.com/mathilde.frchrt/time_series_aims/-/blob/master/inst/sql/cases/case1.sql)

## Study case 2 : Hypotension during the first post-operative ICU stay

| Feature parameters | Values | 
| -------- | -------- | 
| Statistical unit     | ICU stay     |
| Period     | First ICU stay     |
| Signal     | Mean arterial pressure     |
| Extraction method    | Duration below 65 mmHg     |

[SQL code for periods creation](https://gitlab.com/mathilde.frchrt/time_series_aims/-/blob/master/inst/sql/cases/case2.sql)


## Study case 3 : Psychiatric scale after a visit (or new treatment)

During the 4 weeks following a new visit with the psychiatrist (or a new treatment), we need to assess the MADRS scale, and detect episodes below 20.

The period is defined by the event **visit_occurrence** with a psychiatrist (or a new **drug_exposure**) for the start date. The end date is equal to the start date + 4 weeks.

![](https://framagit.org/antoine.lamer/omop_anesthesia/-/raw/master/images/case_study_madrs.png)


| Feature parameters | Values | 
| -------- | -------- | 
| Statistical unit     | Treatment     |
| Period     | 28 days following a new treatment    |
| Signal     | MADRS    |
| Extraction method    |  duration < 20    |

[SQL code for periods creation](https://gitlab.com/mathilde.frchrt/time_series_aims/-/blob/master/inst/sql/cases/case4.sql)

## Study case 4 : Duration of hypotension during general anesthesia (period defined with candidates events)


| Feature parameters | Values | 
| -------- | -------- | 
| Statistical unit     | Anesthesia procedure    |
| Period     | Induction - End of anesthesia  |
| Signal     | Mean arterial pressure    |
| Extraction method    |  duration < 65 mmHg   |

The period may be computed with candidates events: 
- Start : administration of an hypnotic, documentation of induction, tidal volume > 100 ml, intubation
- End : documented of end of anesthesia

[script]


## Study case 5 : Threshold with a relative value

![](https://gitlab.com/antoinelamer/omop_anesthesia/-/raw/master/images/case_study_threshold_relative_value.png)

**Relative** threshold defined as the first MAP measurement at the arrival in the operating room.

Drop above 20% of the relative threshold between induction and birth, during caesarean section procedure.

| Feature parameters | Values | 
| -------- | -------- | 
| Statistical unit     | Caesarean procedure    |
| Period     | Induction - Birth  |
| Signal     | Mean arterial pressure    |
| Extraction method    |  duration < 80% of reference value   |


## Study case 6 : Min/Mean/Max values during algorithmically defined periods

Min/Mean/Max Arterial pressure during four periods : 
- [OR arrival - Induction]
- [Induction - Incision]
- [Incision - Closure]
- [Closure-Release from OR]

These periods may be defined by events from PROCEDURE_OCCURRENCE, OBSERVATION, DRUG_EXPOSURE.

The **aggregation_method_concept_id** are min, mean and max values.

![](https://framagit.org/antoine.lamer/omop_anesthesia/-/raw/master/images/case_study_indicators_perop_map.png)

| Feature parameters | Values | 
| -------- | -------- | 
| Statistical unit     | Caesarean procedure    |
| Period     | Induction - Birth  |
| Signal     | Mean arterial pressure    |
| Extraction method    |  duration < 80% of reference value   |

## Study case 7 : Evolution of MAP around induction

Periods
- induction-15 - induction-10
- induction-10 - induction-5
- induction-5 - induction
- induction - induction+5
- induction+5 - induction+10
- induction+10 - induction+15
- induction+15 - induction+20

The **aggregation_method_concept_id** are min and median values.

![](https://framagit.org/antoine.lamer/omop_anesthesia/-/raw/master/images/case_study_around_event.png)

## Study case 8 - Evolution of MAP after hypotension and administration of noradrenalin

Lors d'un épisode d'hypotension peropératoire, combien de temps faut-il pour que la tension repasse au dessus de 65 mmHg après l'administration de noradrénaline ?

# New tables

script

## PERIOD

The PERIOD table contains time period defined algorithmically with events from the other tables. A period is defined by start and end dates, which may come from two different kinds of events. Periods will be used to compute aggregates.

A period may be a part of a procedure, for example the period between two events from the anesthesia/surgery procedure : induction-incision, incision-closure, closure-release from the operating theatre. These events may come from different tables. For example, induction may be defined by a procedure, an observation or a drug administration.
A period may also be defined by a start event and a duration, for example a visit with a specialist and the next 4 weeks, the administration of a drug and the next 4 hours or the induction event and the next 15 minutes.

| Field | Required | Type |
| -------- | -------- | -------- |
| period_id     | yes     | Integer   |
| person_id     | yes     | Integer   |
| period_concept_id     | yes     | Integer   |
| start_period_event_id     | no     | Integer   |
| start_event_field_concept_id | no | Integer |
| periode_start_date     | no   | Date   |
| periode_start_datetime     | yes     | Datetime   |
| end_period_event_id     | no   | Integer   |
| end_event_field_concept_id | no | Integer |
| periode_end_date     | no     | Date   |
| periode_end_datetime     | yes  | Datetime   |
| duration | no | Integer |
| duration_unit_concept_id | no | Integer |
| visit_occurrence_id     | no     | Integer |
| visit_detail_id     | no   | Integer |
| period_type_concept_id | yes | Integer |

**Conventions**

| No. | Convention description | 
| -------- | -------- | 
| 1     | start_period_event_id and end_period_event_id are the identifier of the event in the source table     | 
| 2     | start_period_field_concept_id is ...   | 
| 3 | Period_type_concept_id => algo 2 événements, période existante (visit_occurrence, visit_detail) |

**Examples**

| period_id | person_id | period_concept_id | periode_start_date | periode_end_date | period_type_concept_id |
| -------- | -------- | -------- |-------- |-------- | -------- |
| 1     | 1     | sejour     | T1 | T2 | séjour existant |
| 2     | 1     | sejour     | T3 |  T4 | séjour existant |
| 3     | 1     | ICU    | T5 | T6 | visit_detail |
| 4     | 1     | anesthesia period  | T7 | T8 | algorithm, 2 events |
| 5     | 1     | anesthesia period  | T9 | T10 | algorithm, 2 events |
| 5     | 2     | anesthesia period  | T9 | T10 | algorithm, 2 events |

- [x] How to defined the source table of a start/end event id ?
- [x] Need for a duration field ?

## FEATURE

The FEATURE table contains aggregated values of measurements, during defined periods and computed with a range of methods (min, max, count, duration, etc ...).

| Field | Required | Type | Description |
| -------- | -------- | -------- | -------- |
| aggregated_measurement_id   | Yes     | integer     | A unique identifier for each aggregated measurement. |
| person_id | Yes | Integer | A foreign key identifier to the Person for whom the visit is recorded. The demographic details of that Person are stored in the PERSON table. |
| period_id     |  No  | Integer | The period identifier during which the aggregation is computed : visit_occurrence_id, visit_detail_id, procedure_occurrence_id, condition_occurrence_id, episode_id |
| aggregated_indicator_concept_id (à supprimer)    | Yes | Integer     | The aggregation indicator identifier. <br />(signal/raw_data + method + period <br /> E.g. Hypotension < 65 mmHg au bloc, MADRS < 20 during the first four weeks following a psychiatrist appointement |
| signal_concept_id or raw_data_concept_id ?    | Yes | Integer | vital signs (e.g. heart rate), psychiatric scale (FAST), biology (creatinine) |
| method_concept_id or aggregation_method_concept_id ?   | Yes   | Integer    | min, max, mean, median, occurrence (Y/N), duration, nb of administrations, sum, number of occurrences, AUC |
| period_concept_id ?     | Yes  | Integer    |  Anesthésie, J0-J3 post-hop, épisode de soins   |
| value_as_number     |     |      |
| value_as_concept_id |     | Integer |
| visit_occurrence_id | No | Integer | A foreign key to the Visit in the VISIT_OCCURRENCE table during which the Procedure was carried out. |
| visit_detail_id | No | Integer |A foreign key to the Visit Detail in the VISIT_DETAIL table during which the Procedure was carried out. |
| aggregated_indicator_source_concept_id     | Yes | Integer     | SIGNAL + METHODE + PERIODE <br /> Hypotension < 65 mmHg au bloc<br /> FAST < seuil dans les 3 mois qui suivent une hospit |
| signal_source_concept_id     | Yes | Integer | vital signs (e.g. heart rate), psychiatric scale (FAST), biology (creatinine) |
| method_source_concept_id     | Yes   | Integer    | min, max, mean, median, occurrence (Y/N), duration, nb of administrations, sum, number of occurrences, AUC |
| period_source_concept_id     | Yes  | Integer    |  Anesthésie, J0-J3 post-hop, épisode de soins   |

**Conventions**

| No. | Convention description | 
| -------- | -------- | 
| 1     |      | 
| 2 |  |

# Extraction functions

## createPeriodParameters

| Field | Column 2 | Column 3 |
| -------- | -------- | -------- |
| period_concept_id    | Text     | Text     |
| event_concept_id    | Integer     | Event which triggers start or end |
| start_end    | Text     | Text     |
| delta    | Text     | Text     |
| delta_unit_concept_id    | Text     | Text     |
| aggregation_rule    | Text     | If multiple candidate events, which one do we choose ?     |
| multiple_period    | Text     | If multiple candidate events, which one do we choose ?     |

- [ ] multiple periods ?


## computePeriod

1) On récupère tous les événements candidats
2) Pour début/fin, on sélectionne l'événement approprié en fonction d'une règles de priorité.
3) Si plusieurs périodes (de même type), définition d'un intervalle maximale entre deux périodes concurrentes.

## createFeatureParameters

1. Statistical unit
2. Period
3. Signal
4. Extraction method

## computeFeatureExtraction

1. Extract signal during period
2. Compute feature

### Extraction methods

| Methods | Description | Column 3 |
| -------- | -------- | -------- |
| Occurrence    | Text     | Text     |
| Count    | Text     | Text     |
| Min    | Text     | Text     |
| Min without extreme values   | Text     | Text     |
| Mean    | Text     | Text     |
| Mean without extreme values    | Text     | Text     |
| Max    | Text     | Text     |
| Max without extreme values    | Text     | Text     |
| Duration outside threshold    | Text     | Text     |

## pivotFeature

Transform row-oriented features into column-oriented features.

Pour le pivot, sur les 3 trois champs concaténés : period + signal + feature method.



# New concepts

OMOP Concept

| Concept | concept_code| Class |
| -------- | -------- | -------- |
| min     | Text     | Method     |
| max     | Text     | Method     |
| mean     | Text     | Method     |
| median     | Text     | Method     |
| q1     | Text     | Method     |
| q3     | Text     | Method     |
| count     | Text     | Method     |
| count distinct     | Text     | Method     |
| sum     | Text     | Method |
| duration     | Text     | Period |
| 1st ICU stay | Text     | Period    |
| 1st post operative ICU stay | Text     | Period    |
| Psychiatric episode of care | Text  | Period     |
| Anesthesia period | Text     | Period |
| Surgery period | Text     | Period |
| 30 minutes following the administration <br />of noradrenalin in operting room | Text     | Period |


[^Adler]: Adler-Milstein J, DesRoches CM, Kralovec P, Foster G, Worzala C, Charles D, et al. Electronic Health Record Adoption In US Hospitals: Progress Continues, But Challenges Persist. Health Aff Proj Hope. déc 2015;34(12):2174‑80. 

[^Safran]: Safran C, Bloomrosen M, Hammond WE, Labkoff S, Markel-Fox S, Tang PC, et al. Toward a National Framework for the Secondary Use of Health Data: An American Medical Informatics Association White Paper. J Am Med Inform Assoc. 1 janv 2007;14(1):1‑9.

[^Safran_2014]: Safran C. Reuse of Clinical Data. Yearb Med Inform. 15 août 2014;9(1):52‑4. 

[^Lamer_2016]: Lamer A, Jeanne M, Marcilly R, Kipnis E, Schiro J, Logier R, et al. Methodology to automatically detect abnormal values of vital parameters in anesthesia time-series: Proposal for an adaptable algorithm. Comput Methods Programs Biomed. Juin 2016;129:160‑71.

[^Fayyad]: Fayyad UM, Piatetsky-Shapiro G, Smyth P. From data mining to knowledge discovery: an overview. In: Advances in knowledge discovery and data mining. USA: American Association for Artificial Intelligence; 1996. p. 1–34.

[^Lamer_2016_2]: Lamer A, Jeanne M, Ficheur G, Marcilly R. Automated Data Aggregation for Time-Series Analysis: Study Case on Anaesthesia Data Warehouse. Stud Health Technol Inform. 2016;221:102‑6. 

[^Chazard_2018]: Chazard E, Ficheur G, Caron A, Lamer A, Labreuche J, Cuggia M, Genin M, Bouzille G, Duhamel A. Secondary Use of Healthcare Structured Data: The Challenge of Domain-Knowledge Based Extraction of Features. Stud Health Technol Inform. 2018;255:15-19.

[^Chazard_2009]: Chazard E, Ficheur G, Merlin B, Genin M, Preda C; PSIP consortium, Beuscart R. Detection of adverse drug events detection: data aggregation and data mining. Stud Health Technol Inform. 2009;148:75-84. PMID: 19745237.